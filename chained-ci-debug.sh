#!/usr/bin/env bash

export DEBUG_FOLDER=${DEBUG_FOLDER:-/tmp/chained-ci-debug}
export SRC_FOLDER=${SRC_FOLDER:-/playbooks}
cd ${SRC_FOLDER}

echo """
##
# Prepare Environment
##
"""

chained-ci-prepare

cd ${DEBUG_FOLDER}
bash chained-ci-debug-run.sh
