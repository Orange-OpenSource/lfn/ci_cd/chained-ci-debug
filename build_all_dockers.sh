#!/usr/bin/env sh
set -e
. ./ansible_versions.sh

# loop on latest ansible versions
for ANSIBLE_VERSION in ${LATEST_MAJOR_VERSIONS}; do
  echo "#######################################################################"
  echo "Build chained-ci-debug ansible version ${ANSIBLE_VERSION}"
  echo "#######################################################################"


  for CI_COMMIT_REF_SLUG in "master" "alpine"; do
    # set image name based on branch
    if [ "${CI_COMMIT_REF_SLUG}" == "master" ]; then
      REF=""
    else
      REF="-${CI_COMMIT_REF_SLUG}"
    fi
    FROM="registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible"
    VERSION="${ANSIBLE_VERSION%.*}${REF}"
    echo "Build based on ${VERSION}"
    sed -i -e 's@^FROM .*$@FROM '"${FROM}"':'"${VERSION}"'@' Dockerfile
    docker build \
      --build-arg FROM=${FROM} \
      -t "${CI_REGISTRY_IMAGE}:${VERSION}" .

    docker push "${CI_REGISTRY_IMAGE}:${VERSION}"

    if [ "${ANSIBLE_VERSION}" == "${LATEST_VERSION}" ]; then
      docker tag "${CI_REGISTRY_IMAGE}:${VERSION}" "${CI_REGISTRY_IMAGE}:latest${REF}";
      docker push "${CI_REGISTRY_IMAGE}:latest${REF}";
    fi
  done
done
