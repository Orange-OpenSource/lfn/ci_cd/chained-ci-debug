#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Chained CI Debug script
"""

import configparser
import errno
import getopt
import os
import re
import shutil
import stat
import sys
import yaml
import requests

FILE_PREFIX = 'chained-ci-debug-'
NONE = 0
DEBUG = 1
LOW = 2
CRITICAL = 3
VERBOSE = LOW


def log(msg, lvl=LOW):
    """
    Loggin function.

    Just to harmonize output

    :param msg: message to ouptut
    :type msg: string
    :param lvl: log level
    :type lvl: int
    """
    if VERBOSE > NONE:
        if lvl == CRITICAL:
            print('!!!', msg)
            sys.exit(2)
        elif lvl >= VERBOSE:
            print('>>>', msg)


def get_local_git_remote():
    """
    Get GIT info from local folder.

    This read the local .git/config folder to fetch the remote origin value

    :return: Project informations (url, server, project, name, group)
    :rtype: dict
    """
    log('Read local GIT config')
    config = configparser.ConfigParser()
    config.read('.git/config')
    url = config['remote "origin"']['url']
    if url.startswith('git'):
        regexp = re.search('^git@(.*):(.*)\.git$', url)
    elif url.startswith('http'):
        regexp = re.search('^http?://(.*)/(.*)\.git$', url)
    return {'url': url,
            'server': regexp.group(1),
            'project': regexp.group(2),
            'name': regexp.group(2).split('/')[-1],
            'group': '/'.join(regexp.group(2).split('/')[0:-1])
            }


def gitlab_api_get(project, path):
    """
    Call the gitlab API and return json.

    :param project: project data
    :type msg: dict
    :param path: url path to GET
    :type path: string

    :return: data
    :rtype: json output
    """
    r = gitlab_api_get_raw(project, path)
    return r.json()


def gitlab_api_dl(project, path, filepath):
    """
    Download a file from Gitlab API.

    :param project: project data
    :type msg: dict
    :param path: url path to GET
    :type path: string
    :param filepath: file path of the file to write
    :type filepath: string
    """
    r = gitlab_api_get_raw(project, path)
    with open(filepath, "wb") as file:
        file.write(r.content)

def gitlab_api_get_raw(project, path):
    """
    Call the gitlab API and return json.

    :param project: project data
    :type msg: dict
    :param path: url path to GET
    :type path: string

    :return: raw data from the GET answer
    :rtype: raw data
    """
    url = "https://{}/api/v4/{}".format(project['server'], path)
    log('GET URL "{}"'.format(url), DEBUG)
    headers= {'PRIVATE-TOKEN': project['api_token']}
    return requests.get(url, headers=headers)


def get_gitlab_project_id(project):
    """
    Call the project ID.

    Call Gitlab API to fetch the project ID from project path

    :param project: project data
    :type project: dict

    :return: the project id
    :rtype: int
    """
    log('Retreive Gitlab project ID')
    path = '/projects?membership&simple&search={}'.format(project['name'])
    data = gitlab_api_get(project, path)
    for prj in data:
        if prj['path_with_namespace'] == project['project']:
            return(prj['id'])


def get_gitlab_job(project, job_id):
    """
    Fetch data linked to the job to debug.

    :param project: project data
    :type project: dict
    :param job_id: job id
    :type job_id: int

    :return: the job data (pipeline id, pipeline ref, commit id, commit author,
                           commit title, stage, name)
    :rtype: dict
    """
    log('Retreive Gitlab job infos')
    path = 'projects/{}/jobs/{}'.format(project['id'], job_id)
    data = gitlab_api_get(project, path)
    return {
            'pipeline': {'id': data['pipeline']['id'],
                         'ref': data['pipeline']['ref']},
            'commit': {'id': data['commit']['id'],
                       'title': data['commit']['title'],
                       'author': data['commit']['author_name']},
            'stage': data['stage'],
            'name': data['name']
            }


def dict2civars(d):
    """
    Create a gitlab variables dict list from dict.

    :param d: variables
    :type d: dict

    :return: variables as dict list
    :rtype: list
    """
    return([{"variable_type": "env_var",
             "key": k,
             "value": v} for k, v in d.items()])


def get_all_variables(project, ci):
    """
    Get all variables used by the job.

    All variables are fetch respecting Gitlab priority:
    https://docs.gitlab.com/ee/ci/variables/#priority-of-environment-variables

    :param project: project data
    :type project: dict
    :param ci: gitlab-ci file
    :type ci: dict

    :return: all variables as dict
    :rtype: dict
    """
    log('Get YAML-defined global variables')
    vars = dict2civars(ci['variables'] if 'variables' in ci else {})
    log('Get YAML-defined job-level variables')
    job = project['job']['name']
    ci = job_extends(ci, job)
    vars += dict2civars(ci[job]['variables'] if 'variables' in ci[job] else {})
    log('Get Group-level variables')
    vars += gitlab_api_get(project,
                           'groups/{}/variables'.format(project['group']))
    log('Get Project-level variables')
    vars += gitlab_api_get(project,
                           'projects/{}/variables'.format(project['id']))
    log('Get Trigger variables')
    vars += gitlab_api_get(project,
                           'projects/{}/pipelines/{}/variables'.format(
                           project['id'],
                           project['job']['pipeline']['id']))
    log('Get CI top variables')
    vars_d = {}
    for var in vars:
        if type(var) is dict and var['variable_type'] == "env_var":
            vars_d[var['key']] = var['value']
    return vars_d


def prepare_env_file(vars):
    """
    Create an env.sh file containing all variables.

    :param vars: project variables
    :type vars: dict
    """
    env_file = FILE_PREFIX+"env.sh"
    log('Write environment variables to {}'.format(env_file))
    with open(env_file, "w") as f:
        print("# Simulate triggered pipeline", file=f)
        print("export {}=\"{}\"".format("CI_PIPELINE_SOURCE", "trigger"),
              file=f)
        print("# Other variables", file=f)
        for k, v in vars.items():
            if k == 'artifacts_bin':
                bin_file = FILE_PREFIX+"artifacts.bin"
                log('Write bin artifact to {}'.format(bin_file))
                with open(bin_file, "w") as fa:
                    print(v, file=fa)
                print("export {}=$(cat {})".format(k, bin_file), file=f)
            else:
                print("export {}=\"{}\"".format(k, v), file=f)


def load_yaml_file(file):
    """
    Read local file yaml file.

    :param file: file to read
    :type file: string

    :return: data structure
    :rtype: dict
    """
    with open(file, 'r') as stream:
        try:
            return(yaml.safe_load(stream))
        except yaml.YAMLError as exc:
            print(exc)


def get_gitlabci():
    """
    Read gitlab-ci local file.

    :return: gitlab-ci structure
    :rtype: dict
    """
    log('Load Gitlab CI file')
    gitlabci = load_yaml_file('.gitlab-ci.yml')
    if 'include' in gitlabci:
        for incl in gitlabci['include']:
            if 'local' in incl:
                log('Load Gitlab CI include file ' + incl['local'])
                gitlabci.update(load_yaml_file(incl['local']))
    return gitlabci


def print_script_block(title, file, cmds=[], label=None ):
    """
    Helper to write homogeneous script block

    :param title: block title
    :type title: string
    :param file: file stream to write
    :type file: stream
    :param cmds: commands to dump
    :type cmds: list
    :param label: the label of the command if user wants to select just a part
    :type label: string
    """
    tab = ''
    if label is not None:
        print("if [[ $labels = *\"{}\"* ]]; then".format(label), file=file)
        tab = '    '
    print("""
echo ""
echo "##"
echo "# {}"
echo "##"
echo ""
""".format(title), file=file)
    for l in cmds:
        print(tab+l, file=file)
    if label is not None:
        print("fi", file=file)


def prepare_script(project, ci, dl_artifacts):
    """
    Write the run.sh file.

    :param project: project data
    :type project: dict
    :param ci: gitlab-ci file
    :type ci: dict
    :param dl_artifacts: job ids of downloaded artifacts
    :type dl_artifacts: list of int
    """
    job = project['job']['name']
    ci = job_extends(ci, job)
    log("Get job before_script")
    before = ci[job]['before_script'] if 'before_script' in ci[job] else \
             ci['before_script'] if 'before_script' in ci else []
    log("Get job script")
    script = ci[job]['script'] if 'script' in ci[job] else []
    log("Get job after_script")
    after = ci[job]['after_script'] if 'after_script' in ci[job] else \
        ci['after_script'] if 'after_script' in ci else []
    run_file = FILE_PREFIX+"run.sh"
    log('Write script to {}'.format(run_file))
    with open(run_file, "w") as f:
        print("#!{}".format(os.getenv('SHELL', '/bin/bash')), file=f)
        print("""
labels=$*
if [[ $labels = "" ]]; then
  labels="before script after"
fi""", file=f)
        print_script_block("Load env vars", f)
        print("source ./{}".format(FILE_PREFIX+"env.sh"), file=f)
        if dl_artifacts:
            print_script_block("Unzip previous jobs artifacts", f)
            for jobid in dl_artifacts:
                print("unzip -o",
                      FILE_PREFIX+"artifact_{}.zip".format(jobid),
                      file=f)
        print_script_block("Before Script", f, before, label='before')
        print_script_block("Script", f, script, label='script')
        print_script_block("After script", f, after, label='after')
        print_script_block("End of job", f, ['exit'])
    os.chmod(run_file, stat.S_IRWXU)


def job_extends(ci, job):
    """
    Extends the ci job with sub parts.

    :param ci: gitlab-ci file
    :type ci: dict
    :param job: job name
    :type file: string

    :return: the ci structure with extended job
    :rtype: dict
    """
    if 'extends' in ci[job]:
        ci = job_extends(ci, ci[job]['extends'])
        ci[job].update(ci[ci[job]['extends']])
    return ci


def prepare_folder(src, dst):
    """
    Create a debug folder, copy of the local git project.

    :param src: the source folder path
    :type src: string
    :param dst: the destination folder path
    :type dst: string
    """
    try:
        shutil.rmtree(dst, ignore_errors=True)
        shutil.copytree(src, dst)
    except OSError as exc: # python >2.5
        if exc.errno == errno.ENOTDIR:
            shutil.copy(src, dst)
        else: raise


def get_artifacts_list(project, gitlabci):
    """
    Get list of jobs id's where we have to download artifacts.

    :param project: project data
    :type project: dict
    :param ci: gitlab-ci file
    :type ci: dict

    :return: jobs id's where we have to download artifacts.
    :rtype: list of int
    """
    stages = gitlabci['stages']
    debug_stage_index = stages.index(project['job']['stage'])
    previous_stages = stages[0:debug_stage_index]
    pipeline_jobs = gitlab_api_get(project,
                                  'projects/{}/pipelines/{}/jobs'.format(
                                       project['id'],
                                       project['job']['pipeline']['id']))
    dl_artifacts = []
    for job in pipeline_jobs:
        if job['stage'] in previous_stages:
            # if we have more than one artifact (log)
            if len(job['artifacts'])>1:
                for artif in job['artifacts']:
                    if artif['file_type'] == 'archive':
                        dl_artifacts.append(job['id'])
    return dl_artifacts


def download_artifacts(project, dl_artifacts):
    """
    Download the pevious artifacts.

    :param project: project data
    :type project: dict
    :param: dl_artifacts: id's where we have to download artifacts.
    :type dl_artifacts: list of int
    """
    for jobid in dl_artifacts:
        filename = FILE_PREFIX+"artifact_{}.zip".format(jobid)
        log("Download " + filename)
        gitlab_api_dl(project,
                      'projects/{}/jobs/{}/artifacts/'.format(
                          project['id'], jobid),
                      filename)


def read_options(argv):
    """
    Read script options for args and env_vars.

    :param args: script arguments
    :type src: list

    :return: option variables (token, jobid, src, folder)
    :rtype: set
    """
    token = os.getenv('PRIVATE_TOKEN', None)
    jobid = os.getenv('JOB_ID', None)
    src = os.getenv('SRC_FOLDER', '/playbooks')
    folder = os.getenv('DEBUG_FOLDER', '/tmp/chained-ci-debug')
    usage = ('USAGE: chained-ci-debug -t <token> -j <jobid> '
             '[-s <source_folder> -d <debug_folder>]')
    try:
        opts, args = getopt.getopt(argv, "hvt:j:s:d:",
                                   ["token=", "jobid=", "src=", "folder"])
    except getopt.GetoptError:
        log(usage, CRITICAL)
    for opt, arg in opts:
        if opt == '-h':
            log(usage, CRITICAL)
        elif opt in ("-t", "--token"):
            token = arg
        elif opt in ("-j", "--jobid"):
            jobid = arg
        elif opt in ("-s", "--src"):
            src = arg
        elif opt in ("-d", "--folder"):
            folder = arg
        elif opt in ("-v", "--verbose"):
            VERBOSE = DEBUG
    if token is None or jobid is None:
        log(usage, CRITICAL)
    return (token, jobid, src, folder)


def main(argv):
    """
    Main.

    :param args: script arguments
    :type src: list
    """
    (token, jobid, src, folder) = read_options(argv)
    log("Launch debug of job {} for with local repository".format(jobid))
    prepare_folder(src, folder)
    os.chdir(folder)
    project = get_local_git_remote()
    project['api_token'] = token
    project['id'] = get_gitlab_project_id(project)
    project['job'] = get_gitlab_job(project, jobid)
    gitlabci = get_gitlabci()
    vars = get_all_variables(project, gitlabci)
    dl_artifacts = get_artifacts_list(project, gitlabci)
    download_artifacts(project, dl_artifacts)
    prepare_env_file(vars)
    prepare_script(project, gitlabci, dl_artifacts)
    log("Debug environment created '{}'".format(folder))
    log("Now run:\n\ncd {}\nbash {}\n".format(folder, FILE_PREFIX+"run.sh"))


if __name__ == "__main__":
    main(sys.argv[1:])
