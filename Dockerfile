ARG FROM="registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible"
ARG VERSION="latest"

FROM registry.gitlab.com/orange-opensource/lfn/ci_cd/docker_ansible:2.7-alpine

MAINTAINER David Blaisonneau <david.blaisonneau@orange.com>

RUN pip install requests
COPY chained-ci-debug.sh /usr/bin/chained-ci-debug
COPY chained-ci-prepare.py /usr/bin/chained-ci-prepare
COPY bashrc /root/.bashrc

WORKDIR /playbooks
CMD ["chained-ci-debug"]
