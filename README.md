# Chained-ci debugger

Welcome to Chained-CI-Debugger. This container will help you to debug a failing
job launched by chained-ci project

## How to start the container

First, go to your local git folder of the project you want to debug.
Check the image tag you are using. Your image is probably something like
`docker-ansible:2.8-alpine` - so just select the tag part after the
semicolon. Then run:

```
cd my_local_project_to_debug
export TAG=2.8-alpine
docker run --rm -ti \
  -v $PWD:/playbooks \
  --env PRIVATE_TOKEN=<your personnal gitlab access token> \
  --env JOB_ID=<your failing job id> \
  registry.gitlab.com/orange-opensource/lfn/ci_cd/chained-ci-debug:${TAG} \
  bash
```

## How to debug quickly

When you are in the container, run all:

```
. chained-ci-debug
```

## How to debug step by step

You can also do it step by step:

```
cd /playbooks
chained-ci-prepare
```


Then go to the debug folder and explore generated files:

```
cd /tmp/chained-ci-debug
ls chained-ci-debug-*
cat chained-ci-debug-env.sh
cat chained-ci-debug-run.sh
```

Then run the command the CI follow

```
./chained-ci-debug-run.sh
```

You can run a specific part of the script by adding tags corresponding to the
part the CI runs (`before` for before-script, `script` for script, `after` for
after-script) as follow:
```
bash chained-ci-debug-run.sh <tags separate by a space>
```

## Debugging procedure

If you want to test a local change in your project folder (the one on your PC,
not the copy in the container), just restart the procedure via
`. chained-ci-debug` or do it step by step.

## chained-ci-prepare options

- __job id__: The failing job id must be set using `-j` option in
  `chained-ci-debug` or `JOB_ID` environment variable.
- __private token__: Your personnal gitlab access token (api) must be set using
  `-t` option
  in `chained-ci-debug` or `PRIVATE_TOKEN` environment variable.
- ___debug folder___: The debug folder can be set with `-d` option in
  `chained-ci-debug` or `DEBUG_FOLDER` environment variable. The default value
  is `/tmp/chained-ci-debug`
- ___source folder___: The source folder where the GIT project is mounted can be
  set using `-s` option in `chained-ci-debug` or `SRC_FOLDER` environment
  variable. The default value is `/playbooks`

Your personnal gitlab token can be found on your gitlab personal settings:
https://mygitlab.me/profile/personal_access_tokens

## How to manage PRIVATE_TOKEN

To set the PRIVATE_TOKEN at each time is pretty anoying, to simplify the
procedure, it is easy to set it to a local file and load it a `docker run`.

```
echo "PRIVATE_TOKEN=xXxXxXxXxXxXxXx" > ~/.config/gitlab-token.list
chmod 600 ~/.config/gitlab-token.list
```

Then you can load `chained-ci-debug` image and just set the JOB_ID

```
docker run --rm -ti \
  -v $PWD:/playbooks \
  --env-file ~/.config/gitlab-token.list \
  --env JOB_ID=<your failing job id> \
  registry.gitlab.com/orange-opensource/lfn/ci_cd/chained-ci-debug:${TAG} \
  bash
```
